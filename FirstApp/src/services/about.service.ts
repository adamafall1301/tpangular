import { Injectable } from "@angular/core";

@Injectable()

export class AboutService{

    info={
        nom:"BigAda",
        email:"bigada@gmail.com",
        tel:778208245
  }
  comments=[
        {date:new Date,message:"A"},
        {date:new Date,message:"B"},
        {date:new Date,message:"C"}
  ]

  addComment(c){
      c.date=new Date;
      this.comments.push(c);
  }
  getAllcomments(){
      return this.comments;
  }

  getAllinfos(){
      return this.info;
  }
}