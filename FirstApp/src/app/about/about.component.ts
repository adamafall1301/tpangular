import { Component, OnInit } from '@angular/core';
import { AboutService } from 'src/services/about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  info:{nom:string,email:string,tel:Number};
  comments=[];

  commentaire={message:"",date:null};

  onAddcommentaire(c)
  {
    this.aboutservice.addComment(c);
    this.commentaire.message="";
    /* this.commentaire={message:"",date:null}; */
  }

  constructor(private aboutservice:AboutService) {

    this.comments=this.aboutservice.getAllcomments();
    this.info=this.aboutservice.getAllinfos();
  }

  ngOnInit() {
  }

}
