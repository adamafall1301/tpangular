import { Component, OnInit } from '@angular/core';
import{Http} from '@angular/http';
import { from } from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  pagePhotos:any;
  constructor(private http:Http) { }

  ngOnInit() {
  }

  onSearch(dataform){

    this.http.get("https://pixabay.com/api/?key=10967308-e8bbfae7adecdf1335218c9c8&q="
    +dataform.motcle+"&per_page=20&page=1&pretty=true")
    .pipe(map(resp=>resp.json()))
    .subscribe(data=>{
      console.log(data);
      this.pagePhotos=data;
    })
  }
}
